# react-native-hooks-basic

#### 介绍
react-native typescript、hooks、react-navigation(5.X) 基础项目搭建  android、ios

#### react-native 基础项目搭建

- 集成typescript <a href="https://juejin.im/post/5ceea3e96fb9a07eb74b1f7b">地址</a>
- 集成react navigation 
  - <a href="https://reactnavigation.org/docs/getting-started">地址</a>
- 集成react-native-vector-icons
  - <a href="https://bin.zmide.com/?p=141">地址</a>
  - <a href="https://juejin.im/post/5ae1685bf265da0b8a675199">自定义字体,使用iconfont字体</a>
- react-native适配安卓ios刘海屏、异形屏方案: <a href="https://blog.csdn.net/u011215669/article/details/104009056">地址</a>
- 数据存储 react-native-storage
