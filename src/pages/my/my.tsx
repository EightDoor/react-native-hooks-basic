
import React, {useContext} from 'react';
import {View, Text} from 'react-native';
import {Button} from 'react-native-elements';
import {AuthContext} from '../../../navigation';

const My = () => {
  const {signOut} = useContext(AuthContext);
  return (
    <View>
      <Text>我的</Text>
      <Button title="退出" onPress={() => signOut()} />
    </View>
  );
};

export default My;
