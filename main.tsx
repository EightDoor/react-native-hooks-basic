import React, {useEffect} from 'react';
import {AppRegistry, AppState} from 'react-native';
import App from './adapter';
import {name as appName} from './app.json';
import store from './src/store/index';
import {Provider} from 'react-redux';

// 引入动态主题Provider
import { ThemeContextProvider } from './src/theme'
import {AppUpgrade} from './src/utils/upgrade';

const Root = () => {
  // 初始化加载
  useEffect(() => {
    AppStatusChange();
  }, []);

  /**
   * 监听app处于前台还是后台，触发app更新
   */
  const AppStatusChange = () => {
    AppState.addEventListener('change', (state) => {
      // app处于前台
      if (state === 'active') {
        console.log(state, 'state');
        AppUpgrade();
      }
    });
  };
  return (
    <ThemeContextProvider>
      <Provider store={store}>
        <App />
      </Provider>
    </ThemeContextProvider>
  );
};
export default Root
