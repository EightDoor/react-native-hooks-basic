import 'react-native-gesture-handler';
import React, {
  useReducer,
  useEffect,
  useMemo,
  createContext,
  useState,
  useContext,
} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {USERTOKEN} from './src/utils/constant';

// 权限验证页面
import Loading from './src/pages/auth/loading';
import Login from './src/pages/auth/login';

// 底部tab
import HomeScreen from './src/pages/home/home';
import MyScreen from './src/pages/my/my';
import AsyncStorage from '@react-native-community/async-storage';

// 业务页面

import HomeAdd from './src/pages/home/home-add';

// @ts-ignore
export const AuthContext = createContext();

const Navigation = () => {
  /**
   * 登录权限验证 开始~
   */
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          AsyncStorage.setItem(USERTOKEN, action.token);
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    },
  );
  // 初始化加载
  useEffect(() => {
    const bootstrapAync = async () => {
      let userToken = null;

      try {
        userToken = await AsyncStorage.getItem(USERTOKEN);
      } catch (e) {}

      dispatch({type: 'RESTORE_TOKEN', token: userToken});
    };
    bootstrapAync();
  }, []);

  const authContext = useMemo(
    () => ({
      signIn: async (data) => {
        dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
      },
      signOut: () => dispatch({type: 'SIGN_OUT'}),
      signUp: async (data) => {
        dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
      },
    }),
    [],
  );
  const Stack = createStackNavigator();
  //   <Stack.Navigator>
  //   <Stack.Screen name="Home" component={HomeScreen} />
  // </Stack.Navigator>
  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {state.isLoading ? (
          <Loading />
        ) : state.userToken == null ? (
          <Login />
        ) : (
          <>
            <Stack.Navigator>
              <Stack.Screen
                name="Index"
                component={TabIndex}
                options={{
                  title: 'TODO LIST APP',
                }}
              />
              <Stack.Screen
                name="HomeAdd"
                options={{title: '添加项'}}
                component={HomeAdd}
              />
            </Stack.Navigator>
          </>
        )}
      </NavigationContainer>
    </AuthContext.Provider>
  );
};
const TabIndex = () => {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      // 自定义底部导航栏
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused
              ? 'ios-information-circle'
              : 'ios-information-circle-outline';
          } else if (route.name === 'My') {
            iconName = focused ? 'ios-list-box' : 'ios-list';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="My" component={MyScreen} />
    </Tab.Navigator>
  );
};
export default Navigation;
