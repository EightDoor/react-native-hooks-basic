
import React, {createContext, useState} from 'react';

// 引入主题文件
import defaultTheme from './default-theme';
import dark from './dart.theme';

const themes = {
  default: defaultTheme,
  dark,
};

export const ThemeContext = createContext();

export const ThemeContextProvider = ({children}) => {
  const [theme, changeTheme] = useState('default');
  return (
    <ThemeContext.Provider
      value={{theme: themes[theme], themeName: theme, changeTheme}}>
      {children}
    </ThemeContext.Provider>
  );
};
