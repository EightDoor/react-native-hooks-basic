
import React, {useState, useContext} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {AuthContext} from '../../../navigation';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const {signIn} = useContext(AuthContext);

  return (
    <View>
      <View style={styles.login}>
        <Text style={styles.loginText}>登录</Text>
        <Input
          value={username}
          onChangeText={setUsername}
          placeholder="请输入用户名"
          leftIcon={<Icon name="user" size={24} color="black" />}
        />
        <Input
          value={password}
          onChangeText={setPassword}
          placeholder="请输入密码"
          leftIcon={<Icon name="user" size={24} color="black" />}
        />
      </View>
      <Button
        title="登录"
        onPress={() => signIn({username, password})}
        containerStyle={styles.submitButton}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  login: {
    marginTop: 100,
  },
  loginText: {
    textAlign: 'center',
    fontSize: 30,
    marginBottom: 20,
  },
  submitButton: {
    margin: 20,
  },
});
export default Login;
