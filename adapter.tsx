/**
 * 适配安卓IOS刘海屏、异形屏方案
 */
import React, {useEffect, useState} from 'react';
import {Platform, SafeAreaView, NativeModules, StatusBar} from 'react-native';
import Navigation from './navigation';

// TODO 待完成 安卓异形屏适配
const App = () => {
  const {StatusBarManager} = NativeModules;
  const [statusBarHeight, setStatusBarHeight] = useState();
  // 获取状态栏高度
  const getHeight = () => {
    let statusBarHeight;
    if (Platform.OS === 'ios') {
      StatusBarManager.getHeight((height: number) => {
        statusBarHeight = height;
      });
    } else {
      statusBarHeight = StatusBar.currentHeight;
      setStatusBarHeight(statusBarHeight);
    }
  };
  // 初始化加载一次
  useEffect(() => {
    getHeight();
  }, []);
  return (
    <SafeAreaView style={{flex: 1}}>
      <Navigation />
    </SafeAreaView>
  );
};

export default App;
