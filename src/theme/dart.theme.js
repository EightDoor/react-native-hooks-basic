

// 深色
const brandPrimary = 'blue';
const white = '#fff';

const colors = {
  container: 20,

  // brand
  brand_primary: brandPrimary,

  // font
  heading_color: 'rgba(255,255,255,0.85)', // 标题色
  text_color: 'rgba(255,255,255,0.65)', // 主文本色
  text_color_secondary: 'rgba(255,255,255,0.45)', // 次文本色

  // navigation
  header_tint_color: white,
  header_text_color: white,
  tabbar_bg: '#1d1e21',
  header_bg: '#000102',

  // background
  page_bg: '#000102',

  // button
  btn_bg: brandPrimary,
  btn_text: white,
};

export default {
  colors,
};
