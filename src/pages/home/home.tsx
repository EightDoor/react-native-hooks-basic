import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ScrollView,
  TouchableHighlight,
  Button,
} from 'react-native';
import {connect, DispatchProp} from 'react-redux';
import { add, del} from '../../store/actions/actions';
import {ToDoType} from '../../store/statesTypes';
type Props = ToDoType & DispatchProp
import http from '../../utils/request';
import { storage } from '../../utils/storage';
import {TODOLIST} from '../../utils/constant';

interface State {
}
interface StateType {
  list: List[];
}
interface List {
  title: string;
  id: string;
}

class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }
  state: StateType = {
    list: [],
  };

  _del = (id: string)=>{
    this.props.dispatch(del(id))
    storage.load(TODOLIST, (res)=>{
      if(res) {
        const data = res.data.list;
        const result = data.filter((item)=>item.id !== id)
        const r = {
          data: {
            list: result
          }
        }
        storage.save(TODOLIST, r)
      }
    })
  }
  _add = (obj)=>{
    this.props.dispatch(add(obj))
  }
  componentDidMount() {
    // storage.remove(TODOLIST)
    this.GetList();
    storage.load(TODOLIST, (v)=>{
      if(v) {
       const list = v.data.list;
       list.forEach((item)=>{
         this._add(item);
       })
      }
    })
  }
  GetList = () => {
    http('http://wx.start6.cn/m').then((res) => {
      this.setState({
        list: res.data,
      });
      console.log(this.state.list, 'list');
    });
  };
  Del = (id: string) => {
    this._del(id)
  };
  Item = ({title, id}) => {
    return (
      <View style={styles.item}>
        <Text>{title}</Text>
        <TouchableHighlight onPress={() => this.Del(id)}>
          <Text style={styles.del} >删除</Text>
        </TouchableHighlight>
      </View>
    );
  };
  Add = () => {
    const {navigation} = this.props;
    navigation.navigate('HomeAdd');
  };
  render() {
    const RenderItem = ({item}: {item: List}) => {
      return <this.Item key={item.id} title={item.title} id={item.id} />;
    };
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.addContainer}>
            <Text style={styles.title}>TODO LIST (redux实现, store初始化读取值)</Text>
            <Button title="添加" onPress={() => this.Add()} />
          </View>
          <FlatList
            data={this.props.list}
            renderItem={RenderItem}
            keyExtractor={(item) => item.id}
          />
          {
            this.props.list.length === 0?
              <Text style={styles.noData}>
                暂无数据
              </Text>:null
          }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 50,
  },
  title: {
    textAlign: 'center',
    fontSize: 12,
  },
  item: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    paddingBottom: 10,
  },
  del: {
    color: 'red',
  },
  addContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 30,
  },
  noData: {
    display: "flex",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    fontSize: 25,
    color: "#ccc"
  }
});
const mapStateToProps = (state:any) => ({
  list: state.todo.list
})

export default connect(mapStateToProps)(Home);
