import {ADD, DEL} from './actionsTypes';

const add = (obj)=>({
    type: ADD,
    list: obj
})
const del = (id)=>({
    type: DEL,
    id,
})

export {add,del}
