import { combineReducers } from 'redux';
import {ADD, DEL} from './actions/actionsTypes';
import {ToDoType} from './statesTypes';

const defaultState: ToDoType ={
    list:[]
}
function todo(state=defaultState,action:any){
switch (action.type) {
    case ADD:
    const result = state.list;
    result.push(action.list)
        return {...state, list: result};
    case DEL:
    const rr = state.list;
    const r = rr.filter((item)=>item.id !== action.id)
    return {...state, list: r}
    default:
        return state;
}
}
export default combineReducers({
    todo:todo
});
